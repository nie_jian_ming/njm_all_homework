﻿@[TOC](文章目录)

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

笔记:
[自定义注解实现IOC容器笔记](https://blog.csdn.net/weixin_39417423/article/details/115055060)
[IOC介绍与源码剖析](https://editor.csdn.net/md/?articleId=115034198)
[AOP介绍与源码剖析](https://editor.csdn.net/md/?articleId=115054195)

# 一、Spring 的优势
<font color=#999AAA >Spring 是⼀个综合性，且有很强的思想性框架，每学习⼀天，就能体会到它的⼀些优势。
- **方便解耦，简化开发**
通过Spring提供的IoC容器，可以将对象间的依赖关系交由Spring进行控制，避免硬编码所造成的过度程序耦合。也不必再为单例模式类、属性文件解析等这些很底层的需求编写代码，可以更专注于上层的应用。
- **AOP编程的⽀持**
通过Spring的AOP功能，方便进行面向切⾯的编程，许多不容易用传统OOP实现的功能可以通过AOP轻松应付。
- **声明式事务的⽀持**
@Transactional 注解可以将我们从单调烦闷的事务管理代码中解脱出来，通过声明式方式灵活的进⾏事务的管理，提高开发效率和质量。
- **方便程序的测试**
可以用非容器依赖的编程方式进行几乎所有的测试工作，测试不再是昂贵的操作，而是随收可做的事情。
- **方便集成各种优秀框架**
Spring可以降低各种框架的使用难度，提供了对各种优秀框架（Struts、Hibernate、Hessian、Quartz等）的直接支持。
- **降低JavaEE API的使用难度**
Spring对JavaEE API（如JDBC、JavaMail、远程调用等）进行了薄薄的封装层，使这些API的使用难度大为降低。
- **源码是经典的 Java 学习范例**
Spring的源代码设计精妙、结构清晰、匠心独用，处处体现着大师对Java设计模式灵活运用以及对Java技术的⾼深造诣。它的源代码无意是Java技术的最佳实践的范例。

# 二、Spring 的核心结构
- **Core Container模块-Spring核心容器（Core Container）**
它管理着Spring应用中bean的创建、配置和管理，是Spring框架最核心的部分。在该模块中，包括了Spring bean工厂，它为Spring提供了DI的功能。基于bean工厂，还有多种Spring应用上下文的实现。所有的Spring模块都构建于核心容器之上。
- **AOP（Aspect Oriented Programming）/Aspects模块 -面向切面**
⾯向切面编程（AOP）/Aspects Spring对面向切面编程提供了丰富的支持。这个模块是Spring应用系统中开发切面的基础，与DI⼀样，AOP可以帮助应面对象解耦。
- **数据处理模块-数据访问与集成（Data Access/Integration）**
Spring的JDBC和DAO模块封装了大量样板代码，这样可以使得数据库代码变得简洁，让开发更专注于业务，还可以避免数据库资源释放失败而引起的问题。 另外，Spring AOP为数据访问提供了事务管理服务，同时Spring还对ORM进行了集成，如Hibernate、MyBatis等。该模块由JDBC、Transactions、ORM、OXM 和 JMS 等模块组成。
- **Web模块**
该模块提供了SpringMVC框架给Web应用，还提供了多种构建和其它应用交互的远程调用方案。 SpringMVC框架在Web层提升了应用的松耦合水平。
- **Test 模块**
为了使得开发者能够很方便的进行测试，Spring提供了测试模块以致力于Spring应用的测试。 通过该模块，Spring为使用Servlet、JNDI等编写单元测试提供了⼀系列的mock对象实现。

# 三、核心思想
<font color=#999AAA >注意：IOC和AOP不是spring提出的，在spring之前就已经存在，只不过更偏向于理论化，spring在技
术层次把这两个思想做了非常好的实现（Java）。
## IOC
### 1. 什么是IOC
IOC Inversion of Control (控制反转/反转控制)，注意它是⼀个技术思想，不是⼀个技术实现。
**IOC做的事情：** Java开发领域对象的创建，管理的问题。
**传统开发方式：** 比如类A依赖于类B，往往会在类A中new⼀个B的对象
**IOC思想下开发方式：** 不用自己去new对象，而是由IOC容器（Spring框架）去实例化对象并且管理它，我们需要使用哪个对象，去问IOC容器要即可。
**例如：** 以前是你自己去找对象一个一个打听兴趣爱好，联系方式等等，想办法认识，然后投其所好等等一系列复杂的事情，自己去设计和面对每一个环节，而现在是引入了第三方婚姻介绍所(IOC容器)，婚姻介绍所有很多男男女女的信息，我们只要告诉它我们的要求，它就会根据我们的要求匹配一个对象给我们，我们只要负责谈恋爱，结婚就可以了。
**利弊：** 我们丧失创建、管理对象的权利,但却不用再去考虑对象的创建、管理等⼀系列事情。

**为什么叫做控制反转？**
控制：指的是对象创建（实例化、管理）的权利
反转：控制权交给外部环境了（spring框架、IoC容器）
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210118151819145.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
### 2. IOC解决了什么问题
IOC解决对象之间的耦合问题
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210118151929768.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
### 3. IOC和DI的区别
DI：Dependancy Injection（依赖注入）
IOC和DI描述的是同⼀件事情，只不过角度不⼀样罢了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210118152200916.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
## AOP
### 1. 什么是AOP
AOP: Aspect oriented Programming 面向切面编程/面向方面编程
AOP是OOP的延续，从OOP说起
OOP三大特征：封装、继承和多态
OOP是⼀种垂直继承体系
![OOP](https://img-blog.csdnimg.cn/2021011909410286.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
OOP编程思想可以解决大多数的代码重复问题，但是有⼀些情况是处理不了的，比如下面的在顶级父类Animal中的多个方法中相同位置出现了重复代码，OOP就解决不了
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210119094149817.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
**横切逻辑代码**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210119094241324.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
横切逻辑代码存在代码重复问题，横切逻辑代码和业务代码混杂在⼀起，代码臃肿，维护不方便AOP出场，AOP独辟蹊径提出横向抽取机制，将横切逻辑代码和业务逻辑代码分析
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210119094435367.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
代码拆分容易，那么如何在不改变原有业务逻辑的情况下，悄无声息的把横切逻辑代码应⽤到原有的业务逻辑中，达到和原来⼀样的效果，这个是比较难的
### 2. AOP在解决什么问题
在不改变原有业务逻辑情况下，增强横切逻辑代码，根本上解耦合，避免横切逻辑代码重复
### 3. 为什么叫做面向切面编程
「切」：指的是横切逻辑，原有业务逻辑代码我们不能动，只能操作横切逻辑代码，所以面向横切逻辑
「面」：横切逻辑代码往往要影响的是很多个方法，每⼀个方法都如同⼀个点，多个点构成面，有⼀个面的概念在里面。
