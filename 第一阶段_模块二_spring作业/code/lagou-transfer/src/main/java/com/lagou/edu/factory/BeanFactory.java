package com.lagou.edu.factory;

import com.alibaba.druid.util.StringUtils;
import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Service;
import com.lagou.edu.annotation.Transactional;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author 聂建明
 * <p>
 * 工厂类，生产对象（使用反射技术）
 */
public class BeanFactory {

    /**
     * 1.扫描包下所有的@service，通过反射完成对象实例化
     * 2.根据@Autowired完成注解的依赖关系
     * 3.针对@Transactional，修改对象为对应的代理对象
     * 4.对外提供获取实例对象的接口（根据id获取）
     */

    private static Map<String, Object> map = new TreeMap<>(String.CASE_INSENSITIVE_ORDER); // 存储对象


    //声明静态方法块，一开始就加载
    //扫描包通过反射技术实例化对象并且存储待用（map集合）
    static {
        try {
            //扫描包获取反射对象集合
        /*
        Reflections通过扫描classpath，索引元数据，允许在运行时查询这些元数据，也可以保存收集项目中多个模块的元数据信息。
        使用 Reflections 可以查询以下元数据信息:
        1.获取某个类型的全部子类;
        2.只要类型、构造器、方法，字段上带有特定注解，便能获取带有这个注解的全部信息（类型、构造器、方法，字段）;
        3.获取所有能匹配某个正则表达式的资源;
        4.获取所有带有特定签名的方法，包括参数，参数注解，返回类型;
        5.获取所有方法的名字;
        6.获取代码里所有字段、方法名、构造器的使用;
         */
            Reflections reflections = new Reflections("com.lagou.edu");
            //获取带有Service注解类型的类
            Set<Class<?>> typesAnnotatedWith = reflections.getTypesAnnotatedWith(Service.class);
            for (Class<?> resourceAsStream : typesAnnotatedWith) {
                //通过反射技术实例化对象
                Object beanObejct = resourceAsStream.getDeclaredConstructor().newInstance();
                //获取注解信息
                Service annotation = resourceAsStream.getAnnotation(Service.class);

                //该实例service注解有用value时用value的值，没有时用类名
                if (StringUtils.isEmpty(annotation.value())) {
                    //由于getName获取的是全限定类名，所以要分割去掉前面包名部分
                    Class<?>[] interfaces = resourceAsStream.getInterfaces();
                    for (Class<?> interfaceClass : interfaces) {
                        String[] names = interfaceClass.getName().split("\\.");
                        map.put(names[names.length - 1], beanObejct);
                    }
                } else {
                    map.put(annotation.value(), beanObejct);
                }
            }

            //实例化对象后，开始维护对象的依赖关系Autowired，检查哪些对象需要传值进入
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Object bean = entry.getValue(); //获取bean
                Class<?> beanClass = bean.getClass(); //获取对应的class
                //获取所有变量
                Field[] declaredFields = beanClass.getDeclaredFields();
                //遍历变量，如果持有Autowired注解则注入
                for (Field field : declaredFields) {
                    //判断这个变量是否使用了注解
                    if (field.isAnnotationPresent(Autowired.class)) {
                        //获取变量的类型名(这里基本等于获取类名)
                        String[] names = field.getType().getName().split("\\.");
                        String name = names[names.length - 1];

                        //用到Autowired地方得有set方法，方便beanClass.getDeclaredMethods()获取
                        Method[] declaredMethods = beanClass.getDeclaredMethods();
                        for (int i = 0; i < declaredMethods.length; i++) {
                            Method declaredMethod = declaredMethods[i];
                            if (declaredMethod.getName().equalsIgnoreCase("set" + name)) {
                                declaredMethod.invoke(bean, map.get(name));
                            }
                        }
                    }
                }
            }

            //维护事务，进行aop切面增强
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                Object bean = entry.getValue(); //获取bean
                Class<?> beanClass = bean.getClass(); //获取对应的class
                //判断对象类是否有Transactional注解，如果有则修改对象为代理对象
                if (beanClass.isAnnotationPresent(Transactional.class)) {
                    //获取代理工厂
                    ProxyFactory proxyFactory = (ProxyFactory) BeanFactory.getBean("proxyFactory");
                    Class<?>[] interfaces = beanClass.getInterfaces(); //获取bean实现的所有接口
                    //判断对象是否实现接口
                    if (null == interfaces || interfaces.length > 0) {
                        //没实现使用CGLIB代理
                        bean = proxyFactory.getCglibProxy(bean);
                    } else {
                        //实现使用JDK代理
                        bean = proxyFactory.getJdkProxy(bean);
                    }

                    //把处理好的bean重新放入map中
                    map.put(entry.getKey(), bean);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    // 对外提供获取实例对象的接口（根据id获取）
    public static Object getBean(String id) {
        return map.get(id);
    }

}
