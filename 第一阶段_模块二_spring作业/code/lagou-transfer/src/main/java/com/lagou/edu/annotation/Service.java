package com.lagou.edu.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 聂建明
 */

/*
关键字@interface，在底层实现上，所有定义的注解都会自动继承java.lang.annotation.Annotation接口。
元注解(专门修饰注解的注解):
@Target注解，用来限定某个自定义注解能够被应用在哪些Java元素上面的。
@Retention注解，用来修饰自定义注解的生命周期,使用了RetentionPolicy枚举类型定义了三个阶段
注解的生命周期有三个阶段：1、Java源文件阶段；2、编译到class文件阶段；3、运行期阶段。
 */
@Target(value = {ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {

    /*
    注解类型元素:
    1.访问修饰符必须为public，不写默认为public;
    2.只能是基本数据类型(不包含对应的封装类型)、String、Class、枚举类型、注解类型(体现了注解的嵌套效果)以及这些类型的数组类型;
    3.该元素的名称一般定义为名词，如果注解中只有一个元素，尽量把名字起为value（后面使用会带来便利操作）;
    4.()不是定义方法参数的地方，也不能在括号中定义任何参数，仅仅只是一个特殊的语法;
    5.default代表默认值，值必须和定义的类型一致;
    6.如果没有默认值，代表后续使用注解时必须给该类型元素赋值;

    注解类型元素的语法非常奇怪，即有属性的特征（可以赋值）,又有方法的特征（打上了一对括号），
    但是这么设计是有道理的，使用的时候操作元素类型像在操作属性，解析的时候操作元素类型像在操作方法。
     */
    String value() default "";

}
