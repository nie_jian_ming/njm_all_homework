﻿@[TOC](文章目录)

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 前言

<font color=#999AAA >文章内容输出来源：拉勾教育Java高薪训练营。P7课程
本篇文章是学习课程中的一部分课后笔记。</font>

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 一、SpringMVC 工作流程
开发过程
1.配置DispatcherServlet前端控制器
2.开发处理具体业务逻辑的Handler（@Controller、@RequestMapping） 
3.xml配置文件配置controller扫描，配置springmvc三大件
4.将xml文件路径告诉springmvc（DispatcherServlet）

## 1.SpringMVC 请求处理流程
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210326143021303.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
**流程说明：**
第一步：用户发送请求至前端控制器DispatcherServlet。
第二步：DispatcherServlet收到请求调用HandlerMapping处理器映射器。
第三步：处理器映射器根据请求Url找到具体的Handler(后端控制器)，生成处理器对象及处理器拦截器(如果有则生成)一并返回DispatcherServlet。
第四步：DispatcherServlet调用HandlerAdapter处理器适配器去调用Handler。
第五步：处理器适配器执行Handler。
第六步：Handler执行完成给处理器适配器返回ModelAndView。
第七步：处理器适配器向前端控制器返回 ModelAndView，ModelAndView 是SpringMVC框架的⼀个底层对象，包括 Model 和 View。
第八步：前端控制器请求视图解析器去进行视图解析，根据逻辑视图名来解析真正的视图。
第九步：视图解析器向前端控制器返回View。
第十步：前端控制器进行视图渲染，就是将模型数据(在 ModelAndView 对象中)填充到 request 域。
第十一步：前端控制器向用户响应结果。

## 1.2 SpringMVC 九大组件

 - **HandlerMapping（处理器映射器）**
HandlerMapping 是用来查找 Handler 的，也就是处理器，具体的表现形式可以是类，也可以是方法。比如标注了@RequestMapping的每个方法都可以看成是⼀个Handler。Handler负责具体实际的请求处理，在请求到达后，HandlerMapping 的作用便是找到请求相应的处理器Handler 和 Interceptor.
 - **HandlerAdapter（处理器适配器）**
 HandlerAdapter 是⼀个适配器。因为 SpringMVC 中 Handler 可以是任意形式的，只要能处理请求即可。但是把请求交给 Servlet 的时候，由于 Servlet 的方法结构都是doService(HttpServletRequest req,HttpServletResponse resp)形式的，要让固定的 Servlet 处理方法调用 Handler 来进行处理，便是 HandlerAdapter 的职责.
 - **HandlerExceptionResolver（异常处理器）**
HandlerExceptionResolver 用于处理 Handler 产生的异常情况。它的作用是根据异常设置ModelAndView，之后交给渲染方法进行渲染，渲染方法会将 ModelAndView 渲染成页面。
 - **ViewResolver（视图解析器）**
ViewResolver用于将String类型的视图名和Locale解析为View类型的视图，只有⼀个resolveViewName()方法。从方法的定义可以看出，Controller层返回的String类型视图名 viewName 最终会在这里被解析成为View。View是用来渲染页面的，也就是说，它会将程序返回的参数和数据填入模板中，生成html文件。
ViewResolver 在这个过程主要完成两件事情：ViewResolver 找到渲染所用的模板和所用的技术(其实也就是找到视图的类型，如JSP)并填入参数。默认情况下，SpringMVC会自动配置⼀个InternalResourceViewResolver,是针对 JSP 类型视图的.
 - **RequestToViewNameTranslator（请求到视图名称的转换）**
RequestToViewNameTranslator 组件的作用是从请求中获取 ViewName.因为 ViewResolver 根据 ViewName 查找 View，但有的 Handler 处理完成之后，没有设置 View，也没有设置 ViewName，便要通过这个组件从请求中查找 ViewName.
 - **LocaleResolver（解析用户区域）**
ViewResolver 组件的 resolveViewName 方法需要两个参数，⼀个是视图名，⼀个是 Locale。
LocaleResolver 用于从请求中解析出 Locale，比如中国 Locale 是 zh-CN，用来表示⼀个区域。这个组件也是 i18n 的基础.
 - **ThemeResolver（主题样式解析器）**
ThemeResolver 组件是用来解析主题的。主题是样式、图片及它们所形成的显示效果的集合。
SpringMVC 中一套主题对应一个 properties文件，里面存放着与当前主题相关的所有资源，如图片、CSS样式等。创建主题非常简单，只需准备好资源，然后新建⼀个“主题名.properties”并将资源设置进去，放在classpath下，之后便可以在页面中使用了。SpringMVC中与主题相关的类有ThemeResolver、ThemeSource和Theme。ThemeResolver负责从请求中解析出主题名，ThemeSource根据主题名找到具体的主题，其抽象也就是Theme，可以通过Theme来获取主题和具体的资源.
 - **MultipartResolver（文件上传解析器）**
MultipartResolver 用于上传请求，通过将普通的请求包装成 MultipartHttpServletRequest 来实现。MultipartHttpServletRequest 可以通过 getFile() 方法直接获得文件。如果上传多个文件，还可以调用 getFileMap() 方法得到Map<FileName，File>这样的结构，MultipartResolver 的作用就是封装普通的请求，使其拥有文件上传的功能.
 - **FlashMapManager（重定向管理）**
 FlashMapManager 就是用来管理 FalshMap 的。
FlashMap 用于重定向时的参数传递，比如在处理用户订单时候，为了避免重复提交，可以处理完post请求之后重定向到⼀个get请求，这个get请求可以用来显示订单详情之类的信息。这样做虽然可以规避用户重新提交订单的问题，但是在这个页面上要显示订单的信息，这些数据从哪里来获得呢？因为重定向时没有传递参数这一功能的，如果不想把参数写进URL(不推荐)，那么就可以通过FlashMap来传递。只需要在重定向之前将要传递的数据写入请求（可以通过ServletRequestAttributes.getRequest()方法获得）的属性OUTPUT_FLASH_MAP_ATTRIBUTE中，这样在重定向之后的Handler中Spring就会自动将其设置到Model中，在显示订单信息的页面上就可以直接从Model中获取数据.
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 二、对 Restful 风格请求支持和Ajax Json交互
## 1.Restful 
Restful 是一种 web 软件架构风格，它不是标准也不是协议，它倡导的是⼀个资源定位及资源操作的风格。本身并没有什么实用性，其核心价值在于如何设计出符合 REST 风格的网络接口。
**Restful 的优点：** 结构清晰、符合标准、易于理解、扩展方便，所以得到越来越多网站的采用。
**RESTful 风格 URL：** 互联网所有的事物都是资源，要求URL中只有表示资源的名称，没有动词。
**RESTful风格资源操作：** 使用HTTP请求中的method方法put、delete、post、get来操作资源。分别对应添加、删除、修改、查询。不过⼀般使用时还是 post 和 get。put 和 delete几乎不使用。
**RESTful 风格资源表述：** 可以根据需求对URL定位的资源返回不同的表述（也就是返回数据类型，比如XML、JSON等数据格式）。
Spring MVC 支持 RESTful 风格请求，具体讲的就是使用 @PathVariable 注解获取 RESTful 风格的请求URL中的路径变量。

## 2.Ajax Json
交互：两个方向
1.前端到后台：前端ajax发送json格式字符串，后台直接接收为pojo参数，使用注解@RequstBody
2.后台到前端：后台直接返回pojo对象，前端直接接收为json对象或者字符串，使用注解
@ResponseBody
### 2.1 Json
Json是⼀种与语言无关的数据交互格式，就是⼀种字符串，只是用特殊符号{}内表示对象、[]内表示数组、""内是属性或值、：表示后者是前者的值。
### 2.2 @ResponseBody注解
@responseBody注解的作用是将controller的方法返回的对象通过适当的转换器转换为指定的格式之后，写入到response对象的body区，通常用来返回JSON数据或者是XML数据。 注意：在使用此注解之后不会再走视图处理器，而是直接将数据写入到输入流中，它的效果等同于通过response对象输出指定格式的数据。
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 三、拦截器(Inteceptor)使用
## 1. 监听器、过滤器和拦截器对比

 - Servlet：处理Request请求和Response响应
 - 过滤器（Filter）：对Request请求起到过滤的作用，作用在Servlet之前，如果配置为/*可以对所有的资源访问（servlet、js/css静态资源等）进行过滤处理。
 - 监听器（Listener）：实现了javax.servlet.ServletContextListener 接口的服务器端组件，它随Web应用的启动而启动，只初始化一次，然后会一直运行监视，随Web应用的停止而销毁。
**作用一：** 做一些初始化工作，web应用中spring容器启动ContextLoaderListener
**作用二：** 监听web中的特定事件，比如HttpSession,ServletRequest的创建和销毁；变量的创建、销毁和修改等。可以在某些动作前后增加处理，实现监控，比如统计在线人数，利用HttpSessionLisener等
 - 拦截器（Interceptor）：是SpringMVC、Struts等表现层框架自己的，不会拦截jsp/html/css/image的访问等，只会拦截访问的控制器方法（Handler）。
从配置的角度能够总结发现：serlvet、filter、listener是配置在web.xml中的，而interceptor是配置在表现层框架自己的配置文件中的。
	* 在Handler业务逻辑执行之前拦截⼀次
	* 在Handler逻辑执行完毕但未跳转页面之前拦截⼀次
	* 在跳转页面之后拦截⼀次
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210326161904948.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
## 2. 拦截器的执行流程
在运行程序时，拦截器的执行是有一定顺序的，该顺序与配置文件中所定义的拦截器的顺序相关。 单个拦截器，在程序中的执行流程如下图所示：
```mermaid
graph TD
 A["CustomInterceptor(proHandle)"] -- return true -->B["HanDlerAdapter(Handle)"] -->C["CustomInterceptor(postHandle)"]-->D["DispatcherServlet(render)"]-->E["CustomInterceptor(afterCompletion)"]
```
1. 程序先执行preHandle()方法，如果该方法的返回值为true，则程序会继续向下执行处理器中的方法，否则将不再向下执行。
2. 在业务处理器（即控制器Controller类）处理完请求后，会执行postHandle()方法，然后会通过DispatcherServlet向客户端返回响应。
3. 在DispatcherServlet处理完请求后，才会执行afterCompletion()方法。

## 3. 多个拦截器的执行流程
多个拦截器（假设有两个拦截器Interceptor1和Interceptor2，并且在配置文件中， Interceptor1拦截器配置在前），在程序中的执行流程如下图所示：
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210326164212737.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
从图可以看出，当有多个拦截器同时工作时，它们的preHandle()方法会按照配置文件中拦截器的配置顺序执行，而它们的postHandle()方法和afterCompletion()方法则会按照配置顺序的反序执行。
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 四、手写MVC框架
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210326164914742.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
```java
  @Override
    public void init(ServletConfig config) throws ServletException {
        // 1 加载配置文件 springmvc.properties
        String contextConfigLocation = config.getInitParameter("contextConfigLocation");
        doLoadConfig(contextConfigLocation);

        // 2 扫描相关的类，扫描注解
        doScan(properties.getProperty("scanPackage"));

        // 3 初始化bean对象（实现ioc容器，基于注解）
        doInstance();

        // 4 实现依赖注入
        doAutoWired();

        // 5 构造一个HandlerMapping处理器映射器，将配置好的url和Method建立映射关系
        initHandlerMapping();

        System.out.println("lagou mvc 初始化完成....");

        // 等待请求进入，处理请求
    }
```
详细代码去[springMvc自定义代码](https://gitee.com/nie_jian_ming/njm_all_homework.git)拉下来查看就好。
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 五、源码剖析和设计模式
笔记地址：[源码剖析和设计模式](https://blog.csdn.net/weixin_39417423/article/details/115250469)

