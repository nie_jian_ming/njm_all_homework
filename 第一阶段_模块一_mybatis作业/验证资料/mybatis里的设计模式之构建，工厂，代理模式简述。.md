﻿@[TOC](文章目录)


<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 前言

<font color=#999AAA >简单记一下mybatis里的设计模式，
Mybatis源码中使用了大量的设计模式，观察设计模式在其中的应用，能够更深入的理解设计模式。
这里介绍的是《第一阶段.模块一Mybatis笔记》这篇文章里那个mybatis自定义框架项目用到的三种设计模式，在实际的mybatis里是什么样子。</font>

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

**Mybatis至少用到了以下的设计模式的使用：**

|  模式  | mybatis 体现|
|--|--|
|Builder模式               |例如SqlSessionFactoryBuilder、Environment;  |
|工厂方法模式  |例如SqlSessionFactory、TransactionFactory、LogFactory；  |
|单例模式   | 例如 ErrorContext 和 LogFactory； |
|代理模式  |  Mybatis实现的核心，比如MapperProxy、ConnectionLogger，用的jdk的动态代理，还有executor.loader包使用了cglib或者javassist达到延迟加载的效果|
|组合模式  |  例如SqlNode和各个⼦类ChooseSqlNode等；|
|模板方法模式  | 例如 BaseExecutor 和 SimpleExecutor，还有 BaseTypeHandler 和所有的子类例如IntegerTypeHandler；|
|适配器模式  | 例如Log的Mybatis接⼝和它对jdbc、log4j等各种日志框架的适配实现； |
|装饰者模式  | 例如Cache包中的cache.decorators⼦包中等各个装饰者的实现； |
|迭代器模式  | 例如迭代器模式PropertyTokenizer； |
<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 一、Builder构建者模式
Builder模式的定义是"将⼀个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表示”，它属于创建类模式，一般来说，如果一个对象的构建比较复杂，超出了构造函数所能包含的范围，就可以使用工厂模式和Builder模式，相对于工厂模式会产出⼀个完整的产品，Builder应用于更加复杂的对象的构建，甚至只会构建产品的⼀个部分，简单来说就是使用多个简单的对象⼀步⼀步构建成⼀个复杂的对象。

## (1).例子：使用构建者设计模式来构建computer(电脑)
 **主要步骤：
 1、将需要构建的目标类分成多个部件(电脑可以分为主机、显示器、键盘、音箱等部件);
 2、 创建构建类;
 3、 依次创建部件;
 4、 将部件组装成目标对象;**
 
 **1. 定义computer(电脑)**
```java
public class Computer {
	  private String displayer;
	  private String mainUnit;
	  private String mouse;
	  private String keyboard;
	  
	  public String getDisplayer() {
	  	return displayer;
	  }
	  public void setDisplayer(String displayer) {
	  	this.displayer = displayer;
	  }
	  public String getMainUnit() {
	  	return mainUnit;
	  }
	  public void setMainUnit(String mainUnit) {
	  	this.mainUnit = mainUnit;
	  }
	  public String getMouse() {
	  	return mouse;
	  }
	  public void setMouse(String mouse) {
	  	this.mouse = mouse;
	  }
	  public String getKeyboard() {
	  	return keyboard;
	  }
	  public void setKeyboard(String keyboard) {
	  	this.keyboard = keyboard;
	  }
	  @Override
	  public String toString() {
		  return "Computer{" + "displayer='" + displayer + '\'' + ", mainUnit='"
		 + mainUnit + '\'' + ", mouse='" + mouse + '\'' + ", keyboard='" + keyboard +
		 '\'' + '}';
  	}
}
```
**2.构建电脑ComputerBuilder**
```java
private ComputerBuilder target = new ComputerBuilder();
	  public Builder installDisplayer(String displayer) {
	  	target.setDisplayer(displayer);
	  	return this;
	  }
	  public Builder installMainUnit(String mainUnit) {
	  	target.setMainUnit(mainUnit);
	  	return this;
	  }
	  public Builder installMouse(String mouse) {
	  	target.setMouse(mouse);
	  	return this;
	  }
	  public Builder installKeybord(String keyboard) {
	  	target.setKeyboard(keyboard);
	  	return this;
	  }
	  public ComputerBuilder build() {
	  	return target;
	  }
}
```
**3.测试**
```java
public static void main(String[]args){
	  ComputerBuilder computerBuilder=new ComputerBuilder();
	  computerBuilder.installDisplayer("显万器");
	  computerBuilder.installMainUnit("主机");
	  computerBuilder.installKeybord("键盘");
	  computerBuilder.installMouse("⿏标");
	  Computer computer=computerBuilder.Builder();
	  System.out.println(computer);
 }
```
## (2).Mybatis中的体现
SqlSessionFactory 的构建过程：
Mybatis的初始化工作非常复杂，不是只用⼀个构造函数就能搞定的。所以使用了建造者模式，使用了大量的Builder，进行分层构造，核心对象Configuration使用了XmlConfigBuilder来进行构造。
![在这里插入图片描述](https://img-blog.csdnimg.cn/2021030910393958.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
在Mybatis环境的初始化过程中，SqlSessionFactoryBuilder会调用XMLConfigBuilder读取所有的MybatisMapConfig.xml 和所有的 *Mapper.xml 文件，构建 Mybatis 运行的核心对象 Configuration对象，然后将该Configuration对象作为参数构建⼀个SqlSessionFactory对象。
```java
private void parseConfiguration(XNode root) {
	try {
		 //issue #117 read properties first
		 //解析<properties />标签
		 propertiesElement(root.evalNode("properties"));
		 // 解析 <settings /> 标签
		 Properties settings = settingsAsProperties(root.evalNode("settings"));
		 //加载⾃定义的VFS实现类
		 loadCustomVfs(settings);
		 // 解析 <typeAliases /> 标签
		 typeAliasesElement(root.evalNode("typeAliases"));
		 //解析<plugins />标签
		 pluginElement(root.evalNode("plugins"));
		 // 解析 <objectFactory /> 标签
		 objectFactoryElement(root.evaINode("obj ectFactory"));
		 // 解析 <objectWrapper Factory /> 标签
		 obj ectWrappe rFacto ryElement(root.evalNode("objectWrapperFactory"));
		 // 解析 <reflectorFactory /> 标签
		 reflectorFactoryElement(root.evalNode("reflectorFactory"));
		 // 赋值 <settings /> 到 Configuration 属性
		 settingsElement(settings);
		 // read it after objectFactory and objectWrapperFactory issue #631
		 // 解析 <environments /> 标签
		 environmentsElement(root.evalNode("environments"));
		 // 解析 <databaseIdProvider /> 标签
		 databaseldProviderElement(root.evalNode("databaseldProvider"));
}
```
其中 XMLConfigBuilder 在构建 Configuration 对象时，也会调用 XMLMapperBuilder 用于读取*Mapper 文件，而XMLMapperBuilder会使用XMLStatementBuilder来读取和build所有的SQL语句。
```java
//解析<mappers />标签
mapperElement(root.evalNode("mappers"));
```
在这个过程中，有⼀个相似的特点，就是这些Builder会读取文件或者配置，然后做大量的XpathParser解析、配置或语法的解析、反射生成对象、存入结果缓存等步骤，这么多的工作都不是⼀个构造函数所能包括的，因此大量采用了 Builder模式来解决。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210309104909355.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
SqlSessionFactoryBuilder类根据不同的输入参数来构建SqlSessionFactory这个工厂对象。

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 二、工厂模式
在Mybatis中比如SqlSessionFactory使用的是工厂模式，该工厂没有那么复杂的逻辑，是⼀个简单工厂模式。
简单工厂模式(Simple Factory Pattern)：又称为静态工厂方法(Static Factory Method)模式，它属于创建者模式。
在简单工厂模式中，可以根据参数的不同返回不同类的实例。简单工厂模式专门定义⼀个类来负责创建其他类的实例，被创建的实例通常都具有共同的父类。

## (1).例子：工厂生产电脑
假设有⼀个电脑的代工生产商，它目前已经可以代工生产联想电脑了，随着业务的拓展，这个代工生产商还要生产惠普的电脑，就需要用⼀个单独的类来专门生产电脑，这就用到了简单工厂模式。
下面我们来实现简单工厂模式：
**1.创建抽象产品类**
创建⼀个电脑的抽象产品类，有⼀个抽象方法用于启动电脑：
```java
public abstract class Computer {
 	//产品的抽象方法，由具体的产品类去实现
 	public abstract void start();
}
```
**2.创建具体产品类**
接着创建各个品牌的电脑，它们都继承了它们的父类Computer，并实现了父类的start方法：

```java
public class LenovoComputer extends Computer{
 	@Override
 	public void start() {
 		System.out.println("联想电脑启动");
	}
}
```
```java
public class HpComputer extends Computer{
	@Override
 	public void start() {
 		System.out.println("惠普电脑启动");
 	} 
}
```
**3.创建工厂类**
接下来创建⼀个工厂类，它提供了⼀个静态方法createComputer用来生产电脑。只需要传入想生产的电脑的品牌，它就会实例化相应品牌的电脑对象。

```java
import org.junit.runner.Computer;
public class ComputerFactory {
	public static Computer createComputer(String type){
 		Computer mComputer=null;
 		switch (type) {
			case "lenovo":
 			mComputer=new LenovoComputer();
 			break;
 			case "hp":
 			mComputer=new HpComputer();
 			break;
 		}
 		return mComputer;
 	}
}
```
**4.客户端调用工厂类**
客户端调用工厂类，传入“hp”生产出惠普电脑并调用该电脑对象的start方法：
```java
public class CreatComputer {
	public static void main(String[]args){
 		ComputerFactory.createComputer("hp").start();
 	} 
}
```
## (2).Mybatis 体现
Mybatis中执行Sql语句、获取Mappers、管理事务的核心接口SqlSession的创建过程使用到了工厂模式。
有⼀个 SqlSessionFactory 来负责 SqlSession 的创建。
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210309112558366.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl8zOTQxNzQyMw==,size_16,color_FFFFFF,t_70)
SqlSessionFactory
可以看到，该Factory的openSession()方法重载了很多个，分别支持autoCommit、Executor、Transaction等参数的输入，来构建核心的SqlSession对象。
在DefaultSqlSessionFactory的默认工厂实现里，有⼀个方法可以看出工厂怎么产出⼀个产品:

```java
private SqlSession openSessionFromDataSource(ExecutorType execType,TransactionIsolationLevel level,boolean autoCommit){
 	Transaction tx=null;
 	try{
		final Environment environment=configuration.getEnvironment();
		final TransactionFactory transactionFactory=
 		getTransactionFactoryFromEnvironment(environment);
		tx=transactionFactory.newTransaction(environment.getDataSource(),level,autoCommit);
		//根据参数创建制定类型的Executor
		final Executor executor=configuration.newExecutor(tx,execType);
 		//返回的是 DefaultSqlSession
 		return new DefaultSqlSession(configuration,executor,autoCommit);
 	}catch(Exception e){
 		closeTransaction(tx); // may have fetched a connection so lets call
 		close()
 		throw ExceptionFactory.wrapException("Error opening session. Cause: "+e,e);
 	}finally{
 		ErrorContext.instance().reset();
 	}
 }
```
这是⼀个openSession调用的底层方法，该方法先从configuration读取对应的环境配置，然后初始化TransactionFactory 获得⼀个 Transaction 对象，然后通过 Transaction 获取一个 Executor 对象，最后通过configuration、Executor、是否autoCommit三个参数构建了 SqlSession。

<hr style=" border:solid; width:100px; height:1px;" color=#000000 size=1">

# 三、代理模式
代理模式(Proxy Pattern):给某⼀个对象提供⼀个代理，并由代理对象控制对原对象的引用。代理模式(Proxy)，它是⼀种对象结构型模式，代理模式分为静态代理和动态代理。

## (1).举例
**1.创建一个抽象类，Person接口，使其拥有一个没有返回值的doSomething方法。**

```java
//抽象类人
public interface Person {
 	void doSomething();
}
```
**2.创建一个名为Bob的Person接口的实现类，使其实现doSomething方法**

```java
//创建⼀个名为Bob的人的实现类
public class Bob implements Person {
 	public void doSomething() {
 		System.out.println("Bob doing something!");
 	} 
}
```
**3.创建JDK动态代理类，使其实现InvocationHandler接口。拥有一个名为target的变量，并创建getTarget获取代理对象方法。**

```java
//JDK动态代理需实现 InvocationHandler 接口/
public class JDKDynamicProxy implements InvocationHandler {
	 //被代理的对象
	 Person target;
	 // JDKDynamicProxy 构造函数
	 public JDKDynamicProxy(Person person) { 
	 	this.target = person;
	 }
	 //获取代理对象
	 public Person getTarget() { 
	 	return (Person) Proxy.newProxylnstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(), this);
	 }
	 //动态代理invoke⽅法
	 public Person invoke(Object proxy, Method method, Object[] args) throws Throwable {
		 //被代理⽅法前执⾏
		 System.out.println("JDKDynamicProxy do something before!");
		 //执⾏被代理的⽅法
		 Person result = (Person) method.invoke(target, args);
		 //被代理⽅法后执⾏
		 System.out.println("JDKDynamicProxy do something after!"); 
		 return result;
 	}
}
```
**4.创建JDK动态代理测试类JDKDynamicTest**

```java
//JDK动态代理测试
public class JDKDynamicTest {
	 public static void main(String[] args) {
		 System.out.println("不使⽤代理类，调⽤doSomething⽅法。");
		 //不使⽤代理类
		 Person person = new Bob();
		 // 调⽤ doSomething ⽅法
		 person.doSomething();
		 System.out.println("分割线-----------");
		 System.out.println("使⽤代理类，调⽤doSomething⽅法。");
		 //获取代理类
		 Person proxyPerson = new JDKDynamicProxy(new Bob()).getTarget();
		 // 调⽤ doSomething ⽅法 proxyPerson.doSomething();
 	} 
}
```
## (2).Mybatis中实现
代理模式可以认为是Mybatis的核心使用的模式，正是由于这个模式，我们只需要编写Mapper.java接口，不需要实现，由Mybatis后台帮我们完成具体SQL的执行。
当我们使用Configuration的getMapper方法时，会调用mapperRegistry.getMapper方法，而该方法又会调用 mapperProxyFactory.newInstance(sqlSession)来生成⼀个具体的代理：

```java
public class MapperProxyFactory<T> {
	 private final Class<T> mapperInterface;
	 private final Map<Method, MapperMethod> methodCache = new
	 ConcurrentHashMap<Method, MapperMethod>();
	 
	 public MapperProxyFactory(Class<T> mapperInterface) {
	 	this.mapperInterface = mapperInterface;
	 }
	 public Class<T> getMapperInterface() {
	 	return mapperInterface;
	 }
	 public Map<Method, MapperMethod> getMethodCache() {
	 	return methodCache;
		@SuppressWarnings("unchecked")
	 	protected T newInstance(MapperProxy<T> mapperProxy) {
	 	return (T)Proxy.newProxyInstance(mapperInterface.getClassLoader(), new Class[] { mapperInterface },mapperProxy);
	 }
	 public T newInstance(SqlSession sqlSession) {
	 	final MapperProxy<T> mapperProxy = new MapperProxy<T>(sqlSession,mapperInterface, methodCache);
	 	return newInstance(mapperProxy);
	 }
 }
```
在这里，先通过T newInstance(SqlSession sqlSession)方法会得到⼀个MapperProxy对象，然后调用TnewInstance(MapperProxy mapperProxy)生成代理对象然后返回。而查看MapperProxy的代码，可以看到如下内容：

```java
public class MapperProxy<T> implements InvocationHandler, Serializable {
	 @Override
	 public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		 try {
		 	if (Object.class.equals(method.getDeclaringClass())) {
		 		return method.invoke(this, args);
		 	} else if (isDefaultMethod(method)) {
		 		return invokeDefaultMethod(proxy, method, args);
		 	}
		 } catch (Throwable t) {
		 	throw ExceptionUtil.unwrapThrowable(t);
		 }
		 final MapperMethod mapperMethod = cachedMapperMethod(method);
		 return mapperMethod.execute(sqlSession, args);
 	}
}
```
非常典型的，该MapperProxy类实现了InvocationHandler接口，并且实现了该接口的invoke方法。通过这种方式，我们只需要编写Mapper.java接口类，当真正执行一个Mapper接口的时候，就会转发给MapperProxy.invoke方法，而该方法则会调用后续的sqlSession.cud>executor.execute>prepareStatement 等⼀系列方法，完成 SQL 的执行和返回。
