package com.njm.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class Configuration {
    //数据源，存放数据库基本信息、Map<唯⼀标识，Mapper> 唯⼀标识：namespace + "." + id
    private DataSource dataSource;
    //map集合：key:statementId value:MappedStatement，
    //sql语句、statement类型、输⼊参数java类型、输出参数java类型
    private Map<String, MappedStatement> mappedStatementMap = new
            HashMap<String, MappedStatement>();

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MappedStatement> getMappedStatementMap() {
        return mappedStatementMap;
    }

    public void setMappedStatementMap(Map<String, MappedStatement> mappedStatementMap) {
        this.mappedStatementMap = mappedStatementMap;
    }
}
