package com.njm.sqlSession;

import com.njm.pojo.MappedStatement;

import java.sql.Connection;
import java.util.List;

public interface Executor {

    <E> List<E> query(Connection connection, MappedStatement mappedStatement, Object[] param) throws Exception;

    int CUD(Connection connection, MappedStatement mappedStatement, Object[] param) throws Exception;

}
