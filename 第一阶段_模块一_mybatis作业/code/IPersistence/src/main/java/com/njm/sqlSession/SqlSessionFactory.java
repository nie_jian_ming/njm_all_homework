package com.njm.sqlSession;

public interface SqlSessionFactory {

    public SqlSession openSession();

}
