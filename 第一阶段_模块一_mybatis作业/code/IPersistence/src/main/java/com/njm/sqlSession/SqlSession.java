package com.njm.sqlSession;

public interface SqlSession {

    //查询所有
//    public <E> List<E> selectList(String statementId, MappedStatement mappedStatement, Object... param) throws Exception;
    //根据条件查询单个
//    public <T> T selectOne(String statementId,MappedStatement mappedStatement,Object... params) throws Exception;
    //生产dao实现,为Dao接口生成代理实现
    public <T> T getMapper(Class<?> mapperClass);

}
