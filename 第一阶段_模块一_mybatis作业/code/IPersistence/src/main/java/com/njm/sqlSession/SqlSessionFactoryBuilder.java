package com.njm.sqlSession;

import com.njm.config.XMLConfigerBuilder;
import com.njm.pojo.Configuration;

import java.io.InputStream;

//工厂构建者，构建者模式
public class SqlSessionFactoryBuilder {

    private Configuration configuration;

    public SqlSessionFactoryBuilder() {
        this.configuration = new Configuration();
    }

    public SqlSessionFactory build(InputStream inputStream) throws Exception {
        //1.使用dom4j解析配置文件，将解析出来的内容封装Configuration中
        XMLConfigerBuilder xmlConfigerBuilder = new XMLConfigerBuilder();
        Configuration configuration = xmlConfigerBuilder.parseConfig(inputStream);
        //2.创建sqlSessionFatory对象:工厂类：生产sqlSession:会话对象
        SqlSessionFactory sqlSessionFactory = new DefaultSqlSessionFactory(configuration);
        return sqlSessionFactory;
    }
}
