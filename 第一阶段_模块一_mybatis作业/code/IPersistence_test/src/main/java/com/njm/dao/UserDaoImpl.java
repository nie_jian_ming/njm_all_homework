package com.njm.dao;

import com.njm.io.Resources;
import com.njm.pojo.User;
import com.njm.sqlSession.SqlSession;
import com.njm.sqlSession.SqlSessionFactory;
import com.njm.sqlSession.SqlSessionFactoryBuilder;

import java.io.InputStream;
import java.util.List;

//public class UserDaoImpl implements IUserDao{

//    @Override
//    public List<User> findAll () throws Exception {
//        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//
//        List<User> userList = sqlSession.selectList("user.selectList");
//        return userList;
//    }
//
//    @Override
//    public User findByCondition(User user) throws Exception {
//        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
//        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
//        SqlSession sqlSession = sqlSessionFactory.openSession();
//
//        User user2 = sqlSession.selectOne("user.selectOne", user);
//        return user2;
//    }
//}
