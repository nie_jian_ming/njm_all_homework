package com.njm.dao;

import com.njm.pojo.User;

import java.util.List;

public interface IUserDao {

    //查询所有用户
    public List<User> findAll() throws Exception;

    //根据条件进行用户查询
    public User findByCondition(User user) throws Exception;

    //根据条件进行用户查询
    public void addUser(User user) throws Exception;

    //根据条件进行用户查询
    public void updateUser(User user) throws Exception;

    //根据条件进行用户查询
    public void deleteUser(int id) throws Exception;
}
