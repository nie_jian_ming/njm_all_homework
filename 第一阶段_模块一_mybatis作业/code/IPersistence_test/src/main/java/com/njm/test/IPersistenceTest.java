package com.njm.test;

import com.njm.dao.IUserDao;
import com.njm.io.Resources;
import com.njm.pojo.User;
import com.njm.sqlSession.SqlSession;
import com.njm.sqlSession.SqlSessionFactory;
import com.njm.sqlSession.SqlSessionFactoryBuilder;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class IPersistenceTest {

    private IUserDao userDao;

    //@Before: 前置通知, 在方法执行之前执行
    @Before
    public void before() throws Exception {
        //解析xml加载成字节输入流
        InputStream resourceAsSteam = Resources.getResourceAsSteam("sqlMapConfig.xml");
        //把解析出来的流，丢给sqlSession工厂构建者，构建出一个sqlSession工厂
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(resourceAsSteam);
        //工厂生产一个sqlSession
        SqlSession sqlSession = sqlSessionFactory.openSession();
        //通过代理模式生成Dao层接口的代理实现类
        userDao = sqlSession.getMapper(IUserDao.class);

    }

    //查询
    @Test
    public void test() throws Exception{
        User user = new User();
        user.setId(1);
        User user2 = userDao.findByCondition(user);
        List<User> all = userDao.findAll();
        System.out.println(user2);
        System.out.println(all);
    }

    //增加
    @Test
    public void addUser() throws Exception{
        User user = new User();
        user.setId(3);
        user.setUsername("张三啊");
        userDao.addUser(user);
    }

    //修改
    @Test
    public void updateUser() throws Exception{
        User user = new User();
        user.setId(3);
        user.setUsername("李四");
        userDao.updateUser(user);
    }

    //删除
    @Test
    public void deleteUser() throws Exception {
        userDao.deleteUser(3);
    }
}
